what is reCAPTCHA ?

reCAPTCHA is a free service to protect your website from spam and abuse. reCAPTCHA uses an advanced risk analysis engine and adaptive captcha to keep automated software from engaging in absive activities on your site. it does this while letting your valid users pass through with ease.

reCAPTCHA offers more than just spam protection. Every time our CAPTCHAs are solved. that
human effort helps digitize text,annotate images, and build machine learning datasets.This in turn helps preserve books,improve maps, and solve hard Al problems.

demo for reCAPTCHA in your website




How to integrate google reCAPTCHA with django ?

step 1 .  Install django-nocaptcha-recaptcha in your Virtual Environment

              pip install django-nocaptcha-recaptcha

step 2 . Configuration

          Add nocaptcha_recaptcha to your INSTALLED_APPS setting

          Add the following to settings.py

         Required settings:

         NORECAPTCHA_SITE_KEY  (string) = the Google provided site_key
         NORECAPTCHA_SECRET_KEY (string) = the Google provided secret_key

         reffer  this link : https://developers.google.com/recaptcha/intro


        Add the field to a form that you want to protect.

        from nocaptcha_recaptcha.fields import NoReCaptchaField

         class DemoForm(forms.Form):

                captcha = NoReCaptchaField()


       Add Google's JavaScript library to your base template or elsewhere, so it is available on the page        containing the django form.

      <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  
      (optional) You can customize the field.

      You can add attributes to the g-recaptcha div tag through the following

      captcha = NoReCaptchaField(gtag_attrs={'data-theme':'dark'}))

      You can override the template for the widget like you would any other django template.
