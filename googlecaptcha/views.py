from django import forms
from nocaptcha_recaptcha.fields import NoReCaptchaField
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.http import HttpResponse
from .models import *
from django.core.urlresolvers import reverse

try:
    from django.utils import simplejson as json
except:
    import simplejson as json

class DemoForm(forms.Form):
    captcha = NoReCaptchaField(gtag_attrs={'data-theme': 'dark'})


# class DemoView(FormView):
#     form_class = DemoForm

#     success_url = reverse_lazy('success')

#     def form_valid(self, form):
#         return super(DemoView, self).form_valid(form)


def DemoView(request ,msg=None):
    form_class = DemoForm()
    variables = RequestContext(request, {'form':form_class,'msg':msg})
    return render_to_response('index.html', variables)


def gooleView(request):

    if request.POST:
        response_data = {}
        if request.is_ajax():

            rp = request.POST.get

            if rp('event_name') and rp('event_palce') and rp('event_date'):

                form = DemoForm(request.POST)

                if form.is_valid():
                    data = GooGLe( event_name=rp('event_name'),event_palce=rp('event_palce'),event_date =rp('event_date'))
                    data.save()
                    response_data['status'] = "success"
                    response_data['result'] = " thank you sucess fully added event with us"
                else:
                    response_data['status'] = "error"
                    response_data['result'] = "captcha missing "
            else:
                response_data['status'] = "error"
                response_data['result'] = "Please fill the form correctly "
        else:
            print "error"
        return HttpResponse(json.dumps(response_data), content_type='application/json')
