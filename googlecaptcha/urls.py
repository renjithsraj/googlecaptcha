
from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from . import views

admin.autodiscover()

urlpatterns = patterns(
    '',

    url(r'^$',views.DemoView,name="index"),

    url(r'^error/(?P<msg>\d+)/$',views.DemoView,name="home"),

    url(r'^success/$', TemplateView.as_view(template_name="success.html"), {},
        name="success"),

    url(r'^gooleView/$', views.gooleView, name='gooleView'),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

