"""
Django settings for googlecaptcha project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

import sys

def root(x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), '..',x)

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), ))
sys.path.insert(0, os.path.join(PROJECT_ROOT, '..', '..'))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_t6^k&&w_6ein+t8jwqo9fvwg&lcqk4j(s7ab*c93)s^@##00('

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'nocaptcha_recaptcha',
    'django_nose',
    'googlecaptcha',
)

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'googlecaptcha.urls'

WSGI_APPLICATION = 'googlecaptcha.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': root('db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

# NORECAPTCHA_SITE_KEY   = "6LdmngMTAAAAADUSbWi9_q1X0qoxaQosCTZT3k10"
# NORECAPTCHA_SECRET_KEY  = "6LdmngMTAAAAAF0eDIxgD_7bEO_gvQMeFaMIkVA6"


NORECAPTCHA_SITE_KEY   = "6LdHpAMTAAAAADayEptgb77jTi4kUzg8x6s1ZWL6"
NORECAPTCHA_SECRET_KEY  = "6LdHpAMTAAAAABWB0PUyTxEtp606kcq_fOZNdc8e"


TEMPLATE_DIRS = [

  root('templates'),
]
